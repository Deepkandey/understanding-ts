// function with different parameters

type Combinable_FO = string | number;
type Numeric_FO = number | boolean;

type Universal_FO = Combinable_FO & Numeric_FO;

function add_FO(a: number, b: number): number;
function add_FO(a: string, b: string): string;
function add_FO(a: Combinable_FO, b: Combinable_FO) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + " " + b.toString();
    }
    return a + b;
}

const result_FO = add_FO(1, 5);
const resultAsString_FO = add_FO('Deepak', 'Rai');

console.log("Result as number: " + result_FO);
console.log("Result as string: " + resultAsString_FO);