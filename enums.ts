enum Role {
  ADMIN,
  READ_ONLY,
  AUTHOR,
}

// enum Role {
//   ADMIN = 5,
//   READ_ONLY,
//   AUTHOR,
// }

const enumObj = {
  name: "Deepak",
  age: 30,
  hobbies: ["Sports", "Cooking"],
  role: Role.ADMIN,
};

if (enumObj.role === Role.ADMIN) {
  console.log("is admin");
}
