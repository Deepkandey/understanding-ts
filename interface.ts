// interface
interface Named_interface {
  readonly name?: string;
  outputName?: string; // ? indicates optional properties
}

interface Greetable_interface extends Named_interface {
  greet(phrase: string): void;
}

class Person_classInt implements Greetable_interface {
  // can implemenet multiple interfaces
  name?: string;
  age: number;

  constructor(name: string, age: number) {
    // name is optional property here
    if (name) {
      this.name = name;
    }

    this.age = age;
  }

  greet(phrase: string): void {
    console.log(phrase + " " + Person_classInt.name);
  }
}

// object of Person_classInt
let person_objInt: Greetable_interface;
person_objInt = new Person_classInt("Manoj", 38);
// person_objInt.name = "Simon"; // read only property
console.log(person_objInt);
person_objInt.greet("Interface method implemented by class");

// object of interface type
let user_objInterface: Greetable_interface;
user_objInterface = {
  name: "Deepak",

  greet(phrase) {
    console.log(phrase);
  },
};

user_objInterface.greet(
  "Method implemented by object of Greetable_interface type"
);

// user_objInterface.name='Ajay'; // not allowed as it is readonly
