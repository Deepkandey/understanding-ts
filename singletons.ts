class Department_Singleton {
  private constructor(private id: number, private name: string) {}

  private static instance: Department_Singleton;

  static getInstance() {
    if (this.instance) {
      return this.instance;
    } else {
      this.instance = new Department_Singleton(24, "Deepak");
      return this.instance;
    }
  }
}

const department_SingleObj_1 = Department_Singleton.getInstance();
const department_SingleObj_2 = Department_Singleton.getInstance();
console.log(department_SingleObj_1);
console.log(department_SingleObj_2);
console.log(
  "Are these objects equal : " +
    (department_SingleObj_1 === department_SingleObj_2)
);
