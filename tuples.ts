// tuples - special array in ts
// when x number of values and their types are known, then it can be used.

const tupleObj: {
  name: string;
  age: number;
  hobbies: string[];
  role: [number, string];
} = {
  name: "Deepak",
  age: 30,
  hobbies: ["Sports", "Cooking"],
  role: [2, "author"],
};

console.log("first role : ",tupleObj.role[0]);