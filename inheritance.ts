class Department_Inherit {
  static fiscal_year = 2022;
  protected employees: string[] = [];

  constructor(private readonly id: number, public readonly name: string) {}

  static createEmployee(name: string) {
    return { name: name };
  }
  // object that calls this method has to be of Department_Inherit type.
  describe(this: Department_Inherit) {
    console.log(`Department (${this.id}) : ${this.name}`);
  }

  addEmployee(employee: string) {
    // this.id = 24;  // not allowed as it is read only property
    this.employees.push(employee);
  }

  printEmployeeInformation() {
    console.log("Total employees : " + this.employees.length);
    console.log(this.employees);
  }
}

// ITDepartment class
class ITDepartment extends Department_Inherit {
  admins: string[];
  constructor(id: number, admins: string[]) {
    super(id, "IT");
    this.admins = admins;
  }
}

// Accounting class
class Accounting extends Department_Inherit {
  private lastReport: string;

  get mostRecentReport() {
    if (this.lastReport) {
      return this.lastReport;
    }
    throw new Error("No report found");
  }

  set mostRecentReport(value: string) {
    if (!value) {
      throw new Error("Please pass in a valid value");
    }
    this.addReport(value);
  }

  constructor(id: number, private reports: string[]) {
    super(id, "Accounting");
    this.lastReport = reports[0];
  }

  addEmployee(employee: string) {
    if (employee === "Max") {
      return;
    }
    this.employees.push(employee);
  }

  addReport(text: string) {
    this.reports.push(text);
    this.lastReport = text;
  }

  printReports() {
    console.log("Total reports : " + this.reports);
  }
}

// IT Department
const iTObj_Inherit = new ITDepartment(24, ["Max"]);
iTObj_Inherit.addEmployee("Max");
iTObj_Inherit.addEmployee("Manu");

const empObj = ITDepartment.createEmployee("Manoj");
console.log(empObj, ITDepartment.fiscal_year);

console.log(iTObj_Inherit);

// Accounting
const accountObj_Inherit = new Accounting(24, []);

accountObj_Inherit.mostRecentReport = "Year End Report";
accountObj_Inherit.addReport("Something went wrong");
console.log("Most recent report : " + accountObj_Inherit.mostRecentReport);

accountObj_Inherit.addEmployee("Max");
accountObj_Inherit.addEmployee("Manu");

accountObj_Inherit.printReports();

console.log(accountObj_Inherit);
