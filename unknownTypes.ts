let userInput: unknown;
let userName: string;

userInput = 5;
userName = "blank";
if (typeof userInput === "string") {
  userName = userInput;
}

console.log("UserInput : ", userInput);
console.log("Username if assigned string : ", userName);
