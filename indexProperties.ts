interface ErrorContainer { // {email: 'Not a valid email', username: 'Must start with a character'}
    //  id: number; not allowed because of number type
    [prop: string]: string;

}

const errorBag: ErrorContainer = {
    username: 'Must start with a character',
    email: "Must have @ in email address"
};