// const names: Array<string> = [];
// names[0].split(' ');

// const promise: Promise<string> = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve("success");
//     }, 2000);
// });

// promise.then(data => {
//     data.split(' ');
// })

// Generic function with constraint
function merge<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

console.log(merge({ name: 'Deepak' }, { age: 31 }));

const mergeObj = merge({ name: 'Deepak' }, { age: 31 });
console.log("Age: " + mergeObj.age);


interface Lengthy {
    length: number;
}

// Another generic function
function countAndPrint<T extends Lengthy>(element: T): [T, string] {
    let descriptionText = "Got no value";
    if (element.length === 1) {
        descriptionText = "Got 1 element";
    } else if (element.length > 1) {
        descriptionText = "Got : " + element.length + " elements";
    }
    return [element, descriptionText];

}

console.log(countAndPrint('Hi there'));

// keyOf Constraint 
function extractAndConvert<T extends object, U extends keyof T>(obj: T, key: U) {
    return "Value: " + obj[key];
}

console.log(extractAndConvert({ name: "Deepak" }, 'name'));

// Generic classes
class DataStorage<T> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        this.data.splice(this.data.indexOf(item), 1)
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Max');
textStorage.addItem('Manu');
textStorage.removeItem('Max');
console.log(textStorage.getItems());

// Generic utility types
interface CourseGoal {
    title: string;
    description: string;
    completeUntil: Date;
}

function getCourseGoal(title: string, description: string, date: Date): CourseGoal {
    let courseGoal: Partial<CourseGoal> = {};
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = date;
    return courseGoal as CourseGoal;

}

const names: Readonly<string[]> = ['Max', 'Anna'];
// names.push('Manu'); // not allowed
console.log(names);