const person: {
  name: string;
  age: number;
} = {
  // const person = {
  name: "Deepak",
  age: 30,
};

console.log(person.name);

// Another example for nested objects

const product = {
  id: "abc1",
  price: 12.99,
  tags: ["great-offer", "hot-and-new"],
  details: {
    title: "Red Carpet",
    description: "A great carpet - almost brand new!",
  },
};
console.log(product.details);
console.log(product.details.description);