type Admin = {
  name: string;
  privilege: string[];
};

type Employee = {
  name: string;
  startDate: Date;
};

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
  name: "Deepak",
  privilege: ["create-server"],
  startDate: new Date(),
};

console.log(e1);

type Combinable_intersect = string | number;
type Numeric = number | boolean;

type Universal = Combinable_intersect & Numeric;

function add_typeGuard(a: Combinable_intersect, b: Combinable_intersect) {
  if (typeof a === "string" || typeof b === "string") { // type Guard using typeof
    return a.toString() + b.toString();
  }
  return a + b;
}

console.log(add_typeGuard(5,2));

type UnknowEmployee = Admin & Employee;

function printEmployee(emp:UnknowEmployee){
    
}