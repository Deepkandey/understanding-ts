const fetchedUserData = {
    id: 'u1',
    name: 'Max',
    job: {
        title: 'CEO',
        description: 'My own company'
    }
};

console.log(fetchedUserData?.job?.title); // optional chaining ?, if it exists then it conitnue further