type Combinable = number | string; // aliases
type ConversionDescriptor = "as-text" | "as-number";

function aliasesFn(
  n1: Combinable,
  n2: Combinable,
  resultConversion: ConversionDescriptor
) {
  let result;
  if (typeof n1 === "number" && typeof n2 === "number") {
    result = n1 + n2;
  } else result = n1.toString() + n2.toString();

  if (resultConversion === "as-number") {
    return +result;
  } else {
    return result.toString();
  }
}

const alias_combinedAges = aliasesFn(30, 26, "as-number");
console.log("Combined Ages : " + alias_combinedAges);

const alias_combinedStringAges = aliasesFn("30", "26", "as-number");
console.log("Combined String Ages : " + alias_combinedStringAges);

const alias_combinedNames = aliasesFn("Deepak", "Rai", "as-text");
console.log("Combined Names : " + alias_combinedNames);
