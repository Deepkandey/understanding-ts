// return number type
function addFn(n1: number, n2: number) {
  return n1 + n2;
}

// return void type
function printResult(n1: number) {
  console.log("Result : " + n1);
}

printResult(addFn(2, 67));

// function as types
// let combinedValues: Function;
let combinedValues: (a1: number, a2: number) => number;

combinedValues = addFn;
// combinedValues = printResult;
// combinedValues = 5 ;
console.log(combinedValues(8, 8));

// callback
function addAndHandle(n1: number, n2: number, cb: (num: number) => void) {
  let result = n1 + n2;
  cb(result);
}

addAndHandle(10, 20, (result) => {
  console.log("Result of callback function as type : " + result);
});
