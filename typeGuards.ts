type Admin_TG = {
    name: string;
    privileges: string[];
};

type Employee_TG = {
    name: string;
    startDate: Date;
};

// interface ElevatedEmployee extends Employee, Admin {}

type ElevatedEmployee_TG = Admin_TG & Employee_TG;

const e1_TG: ElevatedEmployee_TG = {
    name: 'Max',
    privileges: ['create-server'],
    startDate: new Date()
};

type Combinable_TG = string | number;
type Numeric_TG = number | boolean;

type Universal_TG = Combinable_TG & Numeric_TG;

function add_TG(a: Combinable_TG, b: Combinable_TG) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}

type UnknownEmployee_TG = Employee_TG | Admin_TG;

function printEmployeeInformation(emp_TG: UnknownEmployee_TG) {
    console.log('Name: ' + emp_TG.name);
    if ('privileges' in emp_TG) {
        console.log('Priveleges: ' + emp_TG.privileges);
    }
    if ('startDate' in emp_TG) {
        console.log('Start Date: ' + emp_TG.startDate);
    }
}

printEmployeeInformation({ name: 'Manu', startDate: new Date() });

class Car_TG {
    drive() {
        console.log('Driving...');
    }
}

class Truck_TG {
    drive() {
        console.log('Driving a truck...');
    }


    loadCargo(amount: number) {
        console.log('Loading cargo ...' + amount);
    }
}

type Vehicle_TG = Car_TG | Truck_TG;

const v1 = new Car_TG();
const v2 = new Truck_TG();

function useVehicle(vehicle: Vehicle_TG) {
    vehicle.drive();
    if (vehicle instanceof Truck_TG) {
        vehicle.loadCargo(1000);
    }
}

useVehicle(v1);
useVehicle(v2);
