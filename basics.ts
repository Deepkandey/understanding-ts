// 1. Using number types
// adding 2 numbers - type of the paramters are decalared explicitly as number
function add(n1: number, n2: number) {
  /* Javascript way of handling input type issues

    if(typeof n1!==number || typeof n2!==number){
        throw new Error("Incorrect input!");
    }

    */
  return n1 + n2;
}

const number1 = 7;
const number2 = 2.8;

const result = add(number1, number2);
console.log("Sum of 2 numbers : ", result);

// 2. Using boolean and string types
function add_1(n1: number, n2: number, showResult: boolean, phrase: string) {
  if (showResult) {
    const result = n1 + n2;
    console.log(phrase + result);
  } else {
    return n1 + n2;
  }
}

const number1_1 = 7;
const number2_1 = 2.8;
const resultPhrase = "Result is : ";

add_1(number1_1, number2_1, true, resultPhrase);

// 3. type assignment and type inference