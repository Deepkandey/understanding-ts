# This project is about typeScript basics,its features and workflows.

### dev dependencies used:
lite-server

### Commands to run the code:

1. First compile the typescript files at the project level -> tsc. 
   It will generate filename.js files.
2. type command - npm start
3. Check the live output of the js code in browser(localhost:3000) under console tab (browsers do not have in-built support for ts files)
