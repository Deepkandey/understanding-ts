class Department {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  // object that calls this method has to be of Department type.
  describe(this: Department) {
    console.log("Department : " + this.name);
  }
}

let departmentObj = new Department("Accounting");
console.log(departmentObj);
departmentObj.describe();

const accountingCopy = { name: "DUMMY", describe: departmentObj.describe };
accountingCopy.describe();
