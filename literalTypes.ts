function combine_literalFn(
  n1: number | string,
  n2: number | string,
  resultConversion: "as-text" | "as-number" // literal type are specific version of string type.
) {
  let result;
  if (typeof n1 === "number" && typeof n2 === "number") {
    result = n1 + n2;
  } else result = n1.toString() + n2.toString();

  if (resultConversion === "as-number") {
    return +result;
  } else {
    return result.toString();
  }
}

const literal_combinedAges = combine_literalFn(30, 26, "as-number");
console.log("Combined Ages : " + literal_combinedAges);

const literal_combinedStringAges = combine_literalFn("30", "26", "as-number");
console.log("Combined String Ages : " + literal_combinedStringAges);

const literal_combinedNames = combine_literalFn("Deepak", "Rai", "as-text");
console.log("Combined Names : " + literal_combinedNames);
