class Department_AM {
  private employees: string[] = [];

  constructor(public name: string) {
    // shorthand initialisation of members
  }

  // object that calls this method has to be of Department type.
  describe(this: Department) {
    console.log("Department : " + this.name);
  }

  addEmployee(employee: string) {
    this.employees.push(employee);
  }

  printEmployeeInformation() {
    console.log("Total employees : " + this.employees.length);
    console.log(this.employees);
  }
}

let departmentObj_AM = new Department_AM("Accounting");
departmentObj_AM.addEmployee("Max");
departmentObj_AM.addEmployee("Manu");
// departmentObj_AM.employees[2] = "Deepak"; // not allowed because of private members

console.log(departmentObj_AM);

departmentObj_AM.printEmployeeInformation();
