type AddFn = (a: number, b: number) => number;

let addFunc: AddFn;
addFunc = (a: number, b: number) => {
  return a + b;
};

console.log(addFunc(3, 4));

// same thing using interface
interface AddFnUsingInterface {
  (a: number, b: number): number;
}

let addFuncFromInterface: AddFn;
addFuncFromInterface = (a: number, b: number) => {
  return a + b;
};

console.log(addFuncFromInterface(3, 4));
