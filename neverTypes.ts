// never return type
function generateError(message: string, code: number) : never{
  throw { message: message, errorCode: code };
}

generateError("Uncaught exception",500);
console.log(generateError("Uncaught exception",500));