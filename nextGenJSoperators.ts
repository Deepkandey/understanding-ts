// spread operator
const array_Spread = ["Sports", "Hiking", "Cricket"];

console.log(...array_Spread);
console.log(array_Spread);

const personObj = {
  userName: "Deepak",
  age: 30,
};

const anotherObj = personObj;
const anotherObj1 = { ...personObj };

console.log(anotherObj);
console.log(anotherObj1);

// Rest operator
const add_UsingRest = (...numbers: number[]) => {
  return numbers.reduce((curResult, curValue) => {
    return curResult + curValue;
  }, 0);
};

const addedNumbers = add_UsingRest(1, 2, 3, 4);
console.log("Sum using Rest: " + addedNumbers);

// array and object destructuring
const [hobby1, hobby2, ...remaining] = array_Spread;
console.log(hobby1 + " " + hobby2);

const { userName: firstName, age } = personObj;
console.log(firstName + " " + age);