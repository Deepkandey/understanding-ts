function combine_UnionFn(n1: number | string, n2: number | string) {
  let result;
  if (typeof n1 === "number" && typeof n2 === "number") {
    result = n1 + n2;
  } else result = n1.toString() + n2.toString();

  return result;
}

const union_combinedAges = combine_UnionFn(30, 26);
console.log("Combined Ages : " + union_combinedAges);

const union_combinedNames = combine_UnionFn("Deepak", "Rai");
console.log("Combined Names : " + union_combinedNames);
