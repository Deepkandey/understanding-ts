abstract class Department_Abstract {
  protected employees: string[] = [];

  constructor(protected readonly id: number, public readonly name: string) {}

  // object that calls this method has to be of Department_Abstract type.
  abstract describe(this: Department_Abstract): void;

  addEmployee(employee: string) {
    // this.id = 24;  // not allowed as it is read only property
    this.employees.push(employee);
  }

  printEmployeeInformation() {
    console.log("Total employees : " + this.employees.length);
    console.log(this.employees);
  }
}

// ITDepartment class
class ITDepartment_Abstract extends Department_Abstract {
  admins: string[];

  constructor(id: number, admins: string[]) {
    super(id, "IT");
    this.admins = admins;
  }

  describe() {
    console.log("IT Departmernt - ID: " + this.id);
  }
}

// Accounting class
class Accounting_Abstract extends Department_Abstract {
  constructor(id: number) {
    super(id, "Accounting");
  }

  describe() {
    console.log("Accounting Department - ID " + this.id);
  }
}

// IT Department
const iTObj_Abstract = new ITDepartment_Abstract(24, ["Max"]);
iTObj_Abstract.addEmployee("Max");
iTObj_Abstract.addEmployee("Manu");

iTObj_Abstract.describe();
console.log(iTObj_Abstract);

// Accounting
const accountObj_Abstract = new Accounting_Abstract(28);
accountObj_Abstract.addEmployee("Max");
accountObj_Abstract.addEmployee("Manu");

console.log(accountObj_Abstract);
accountObj_Abstract.describe();
