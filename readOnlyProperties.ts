class Department_RO {
  private employees: string[] = [];
  private readonly id: number;

  constructor(public readonly name: string, id: number) {
    this.id = id;
  }

  // object that calls this method has to be of Department type.
  describe(this: Department_RO) {
    console.log(`Department (${this.id}) : ${this.name}`);
  }

  addEmployee(employee: string) {
    // this.id = 24;  // not allowed as it is read only property
    this.employees.push(employee);
  }

  printEmployeeInformation() {
    console.log("Total employees : " + this.employees.length);
    console.log(this.employees);
  }
}

let departmentObj_RO = new Department_RO("Accounting", 24);
departmentObj_RO.addEmployee("Max");
departmentObj_RO.addEmployee("Manu");

console.log(departmentObj_RO);
