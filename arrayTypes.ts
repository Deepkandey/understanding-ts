// const person: {
//   name: string;
//   age: number;
//   hobbies: string[];
// } = {
//   name: "Deepak",
//   age: 30,
//   hobbies: ["Sports", "Cooking"],
// };

const arrayObj = {
  name: "Deepak",
  age: 30,
  hobbies: ["Sports", "Cooking"],
};

console.log("First hobby : " + arrayObj.hobbies[0]);

let favoriteActivities: string[];
favoriteActivities = ["Sports"];

for (const hobby of arrayObj.hobbies) {
  console.log(hobby.toUpperCase());
}
